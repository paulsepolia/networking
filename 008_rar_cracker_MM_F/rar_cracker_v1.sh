#!/bin/bash

# create the passwords
# the passwords is 12345

PASSWORDS=
for i in {10000..20000}
do
    pw=$(printf "%05d" $i)
    PASSWORDS+="$pw "
done

# enter and check each passwords

for PASSWORD in $PASSWORDS
do
    rar x -p"$PASSWORD" test.rar > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        echo "Password is '$PASSWORD'"
        break
    fi
done
