//================//
// signal handler //
//================//

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

using std::cout;
using std::endl;

// function declaration

void sigalrm_handler(int);

// the main function

int main()
{   
    signal(SIGALRM, sigalrm_handler);

    alarm(2);

    while(true){}

    return 0;
}

// function definition

void sigalrm_handler(int sign)
{   
    cout << "I am alive. Catch the sigalrm " << sign << endl;
    alarm(2);
}

// end

