//======================//
// rar password cracker //
//======================//

#include <iostream>
#include <string>
#include <cmath>

using std::cout;
using std::endl;
using std::string;
using std::to_string;
using std::pow;

// tha maib function

int main()
{
     // input

     const string rar_file_name = "test.rar";

     // fixed input

     const string s1 = "rar t -p";
     const string s2 = "> /dev/null 2>&1";
     string pass = "";
     string total_phrase = "";
     const int I_MAX = 7;
     const unsigned long int I_STEP = 1000UL;
     unsigned long int iL = 0;

     // check password

     do {
          //cout << " ---> 1" << endl;

          iL++;

          pass = to_string(iL);

          //cout << " pass = " << pass << endl;
          //cout << " ---> 2" << endl;

          total_phrase = s1 + pass + " " + rar_file_name + " " + s2;

          //cout << " total_phrase = " << total_phrase << endl;

          system(total_phrase.c_str());

          if(iL%I_STEP == 0) {
               cout << " passwords checked = " << iL << endl;
          }

          //cout << " ---> 3" << endl;

          pass = "";
     } while(iL <= pow(10.0, I_MAX));

     return 0;
}

// end
