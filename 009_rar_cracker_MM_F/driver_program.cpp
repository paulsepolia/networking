//======================//
// rar password cracker //
//======================//

#include <iostream>
#include <string>
#include <algorithm>

using std::cout;
using std::endl;
using std::string;
using std::next_permutation;
using std::sort;
using std::to_string;

// tha maib function

int main()
{
     // input

     const string rar_file_name = "test.rar";
     int my_ints[] = {1,2,3,4,5,6,7};

     // fixed input

     const string s1 = "rar x -p";
     const string s2 = "> /dev/null 2>&1";
     string pass = "";
     string total_phrase = "";
     const int I_MAX = 7;
     const unsigned long int I_STEP = 1000UL;
     unsigned long int iL = 0;

     // sort

     sort(my_ints, my_ints+I_MAX);

     // check password

     do {
          //cout << " ---> 1" << endl;

          //iL++;

          for(int i = 0; i != I_MAX; i++) {
               pass = pass + to_string(my_ints[i]);
          }

          //cout << " pass = " << pass << endl;
          //cout << " ---> 2" << endl;

          total_phrase = s1 + pass + " " + rar_file_name + " " + s2;

          //cout << " total_phrase = " << total_phrase << endl;

          system(total_phrase.c_str());

          //if(iL%I_STEP == 0)
          //{
          //	cout << " passwords checked = " << iL << endl;
          //}

          //cout << " ---> 3" << endl;

          pass = "";
     } while(next_permutation(my_ints, my_ints+I_MAX));

     return 0;
}

// end
