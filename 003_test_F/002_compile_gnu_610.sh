#!/bin/bash

  # 1. compile

  g++-6.1.0 -O3                \
            -Wall              \
            -std=gnu++14       \
            driver_program.cpp \
            -o x_gnu_610
