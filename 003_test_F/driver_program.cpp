//================//
// signal handler //
//================//

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

using std::cout;
using std::endl;

// function declaration

void sigalrm_handler(int);

// the main function

int main()
{
     cout << " --> 1 --> before signal()" << endl;

     signal(SIGALRM, sigalrm_handler);

     cout << " --> 2 --> after signal --> before alarm(2)" << endl;

     alarm(2);

     cout << " --> 3 --> after alarm(2)" << endl;

     while(true) {}

     cout << " --> 4" << endl;

     return 0;
}

// function definition

void sigalrm_handler(int sign)
{
     cout << " --> 5 --> inside function" << endl;
     cout << "I am alive. Catch the sigalrm " << sign << endl;
     cout << " --> 6 --> inside function --> before alarm(6)" << endl;
     alarm(6);
     cout << " --> 7 --> inside function --> after alarm(6)" << endl;
}

// end

