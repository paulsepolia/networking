//================//
// signal handler //
//================//

#include <iostream>
#include <signal.h>
#include <unistd.h>
#include <iomanip>

using std::cout;
using std::endl;
using std::fixed;
using std::setprecision;
using std::setw;
using std::right;

volatile sig_atomic_t print_flag = false;

void handle_alarm(int sig)
{
     print_flag = true;
}

int main()
{
     signal(SIGALRM, handle_alarm); // install handler first,
     alarm(1); // before scheduling it to be called

     long double id = 0.0;

     cout << fixed;
     cout << setprecision(5);

     for (;;) {
          id++;
          if (print_flag) {
               cout << " --> Hello --> " << right << setw(20) << id << endl;
               print_flag = false;
               alarm(2);
          }
     }
}

// end

