//================//
// signal handler //
//================//

#include <iostream>
#include <signal.h>
#include <unistd.h>
#include <iomanip>

using std::cout;
using std::endl;
using std::fixed;
using std::setprecision;
using std::setw;
using std::right;

// function

void display_message(int s)
{
     cout << " --> Still working..." << endl;
}

// the main function

int main()
{
     int ret;

     while(true) {
          signal(SIGALRM, display_message);
          alarm(10);

          if ((ret = sleep(20)) != 0) {
               cout << " --> sleep was interrupted by SIGALRM" << endl;
          }
     }

     return 0;
}

// end

